import sys
import pygame

# --------------------------------------------------------------------------------------------

if not pygame.font:
    print('WARNING: font disabled')
if not pygame.mixer:
    print('WARNING: sound disabled')

# --------------------------------------------------------------------------------------------

from .GameBoard import GameBoard
from .config import *

# --------------------------------------------------------------------------------------------


class GameMain(object):

    def __init__(self, width=SCREEN_WIDTH, height=SCREEN_HEIGHT):

        self.width = width
        self.height = height
        self.time_passed = 0

        pygame.init()
        self.screen = pygame.display.set_mode((width, height))  # , pygame.FULLSCREEN )
        pygame.display.set_caption('pyKulki')

        self.board = GameBoard(self.screen)
        self.clock = pygame.time.Clock()

    def draw(self):
    
        self.board.draw()
        pygame.display.flip()

        if SHOW_FPS:
            pygame.display.set_caption('pyKulki - (FPS:{:d})'.format(int(self.clock.get_fps())))

    def run(self):
    
        while True:

            self.time_passed = self.clock.tick(FPS)
            self.draw()

            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    sys.exit(0)
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        sys.exit(0)
                elif event.type == pygame.MOUSEMOTION:
                    pass
                elif event.type == pygame.MOUSEBUTTONUP:
                    self.board.handle_mouse(event.pos)
