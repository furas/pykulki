import os
import random
import pygame

# --------------------------------------------------------------------------------------------

if not pygame.font:
    print('WARNING: font disabled')
if not pygame.mixer:
    print('WARNING: sound disabled')

# --------------------------------------------------------------------------------------------

from .config import *

# --------------------------------------------------------------------------------------------
# constants
# --------------------------------------------------------------------------------------------

RED   = (255,   0,   0)
GREEN = (  0, 255,   0)
BLUE  = (  0,   0, 255)
WHITE = (255, 255, 255)

MAX_COLORS = 7          # ilosc kolorow

BOARD_X = 9             # szerokosc planszy w klockach
BOARD_Y = 9             # wysokosc planszy w klockach

BOARD_OFFSET_X = 30     # pozycja planszy w oknie
BOARD_OFFSET_Y = 20     # pozycja planszy w oknie

FIELD_SIZE = 48         # wielkosc klocka

NEXT_BLOCKS_NUMBER = 3  # ilosc losowanych klockow
NEXT_BLOCKS_OFFSET_X = BOARD_OFFSET_X + (BOARD_X * FIELD_SIZE) + 30  # pozycja wylosowanych klockow
NEXT_BLOCKS_OFFSET_Y = BOARD_OFFSET_Y  # pozycja wylosowanych klockow

FIELD_EMPTY = 0
FIELD_TESTED = 99  # oznaczenie klocka jako sprawdzonego przy szukaniu trasy

# --------------------------------------------------------------------------------------------


class GameBoard(object):

    def __init__(self, screen):

        self.screen = screen

        # image: background
        self.background = pygame.image.load(os.path.join(DATA_DIR, "bacowka.jpg")).convert()

        # image: selected
        self.sel = pygame.image.load(os.path.join(DATA_DIR, "cloud.png")).convert()
        colorkey = self.sel.get_at((0, 0))
        self.sel.set_colorkey(colorkey, pygame.RLEACCEL)

        # blocks
        self.blocks = []

        # image: block[0]
        img = pygame.image.load(os.path.join(DATA_DIR, "blue_block.png")).convert()
        img = pygame.Surface((FIELD_SIZE-2, FIELD_SIZE-2))
        img.fill(WHITE)
        img.set_alpha(192) #240)
        self.blocks.append(img)

        # image: block[1..MAX_COLORS + 1]
        for i in range(1, MAX_COLORS+1):
            img = pygame.image.load(os.path.join(DATA_DIR, "block_{}.png".format(i))).convert()
            img = pygame.Surface((FIELD_SIZE-2, FIELD_SIZE-2))
            pygame.draw.circle(img, BLUE, ((FIELD_SIZE-6)//2, (FIELD_SIZE-6)//2), (FIELD_SIZE-10)//2)
            img.set_colorkey(img.get_at((0, 0)), pygame.RLEACCEL)
            self.blocks.append(img)

        # -----

        self.score = 0
        self.board = None
        self.skip = 0  # skip next blocks

        # cell selected by mouse
        self.selected = None

        # tablica na losowane klocki
        self.next_blocks = [FIELD_EMPTY] * NEXT_BLOCKS_NUMBER

        # reset game
        self.start()

    def start(self):

        # clear score
        self.score = 0

        # clear board
        self.board = [[FIELD_EMPTY] * BOARD_X for _ in range(BOARD_Y)]

        # select first blocks
        self.get_next_colors()

        # put blocks on board
        self.put_next_blocks()

        # select next blocks
        self.get_next_colors()

    def draw(self):

        # background
        self.screen.blit(self.background, (0, 0))

        # board and blocks
        for row in range(BOARD_Y):
            for col in range(BOARD_X):

                x = BOARD_OFFSET_X + col * FIELD_SIZE
                y = BOARD_OFFSET_Y + row * FIELD_SIZE

                # board
                self.screen.blit(self.blocks[0], (x, y))

                # blocks
                block = self.board[row][col]
                if block != FIELD_EMPTY and block != FIELD_TESTED:
                    self.screen.blit(self.blocks[block], (x+2, y+2))

        # selected
        if self.selected:
            x = BOARD_OFFSET_X + self.selected[0] * FIELD_SIZE + 2
            y = BOARD_OFFSET_Y + self.selected[1] * FIELD_SIZE + 2
            self.screen.blit(self.sel, (x, y))

        # next bloks
        for i in range(NEXT_BLOCKS_NUMBER):
            x = NEXT_BLOCKS_OFFSET_X + i * FIELD_SIZE
            y = NEXT_BLOCKS_OFFSET_Y
            self.screen.blit(self.blocks[0], (x, y))
            self.screen.blit(self.blocks[self.next_blocks[i]], (x+2, y+2))

        # score
        if pygame.font:
            font = pygame.font.Font(None, 36)
            text = font.render(str(self.score), 1, (255, 255, 255))
            self.screen.blit(text, (NEXT_BLOCKS_OFFSET_X, NEXT_BLOCKS_OFFSET_Y + 50))

    # --------------------------------------------------------------------------------------------

    def get_next_colors(self, number=NEXT_BLOCKS_NUMBER):

        random.seed()
        for i in range(number):
            self.next_blocks[i] = random.randint(1, MAX_COLORS)

    # --------------------------------------------------------------------------------------------

    def put_next_blocks(self):

        for next_block in self.next_blocks:

            # losowanie miejsca
            row = random.randrange(BOARD_Y)
            col = random.randrange(BOARD_X)

            # jesli zajete losowanie ponownie
            while self.board[row][col] != FIELD_EMPTY:
                row = random.randrange(BOARD_Y)
                col = random.randrange(BOARD_X)

            # wstawianie koloru
            self.board[row][col] = next_block
            self.remove_all_blocks(col, row)

    # --------------------------------------------------------------------------------------------

    def handle_mouse(self, pos):

        # przeliczanie pikseli na numer pola
        x, y = pos
        x = (x - BOARD_OFFSET_X) // FIELD_SIZE
        y = (y - BOARD_OFFSET_Y) // FIELD_SIZE

        print('handle_mouse pos:', x, y)

        # sprawdzanie czy nie kliknieto poza plansze
        if -1 < x < BOARD_X and -1 < y < BOARD_Y:

            # zaden klocek nie jest zaznaczony
            if not self.selected:

                if self.board[y][x] != FIELD_EMPTY:
                    self.selected = (x, y)

            # jakis klocek jest juz zaznaczony
            else:
                # kliknieto w puste pole
                if self.board[y][x] == FIELD_EMPTY:

                    sel_x, sel_y = self.selected
                    # sprawdzamy czy jest droga od klocka do tego pola
                    if self.check_path(sel_x, sel_y, x, y):
                        # przenosimy klocek w nowe miejsce
                        self.board[y][x] = self.board[sel_y][sel_x]
                        self.board[sel_y][sel_x] = FIELD_EMPTY
                        # kasujemy zaznaczenie
                        self.selected = None
                        # sprawdzamy czy usunac klocki
                        self.remove_all_blocks(x, y)
                        if self.skip == 0:
                            # wstawiamy nowe klocki na plansze
                            self.put_next_blocks()
                            # losujemy nowe klocki do nastepego razu
                            self.get_next_colors()
                        elif self.skip > 0:
                            self.skip -= 1

                # kliknieto w klocek
                else:
                    # klocek juz zaznaczony - odznacz
                    if self.selected == (x, y):
                        self.selected = None
                    # inny klocek - przenies zaznaczenie na niego
                    else:
                        self.selected = (x, y)

    # --------------------------------------------------------------------------------------------
    # sprawdzanie drogi do miejsca docelowowego
    # --------------------------------------------------------------------------------------------

    def check_path(self, x1, y1, x2, y2):
        if self.check_cell(x1, y1-1, x2, y2) \
                or self.check_cell(x1, y1+1, x2, y2) \
                or self.check_cell(x1-1, y1, x2, y2) \
                or self.check_cell(x1+1, y1, x2, y2):
            self.remove_marks()
            return True
        else:
            self.remove_marks()
            return False

    # --------------------------------------------------------------------------------------------
    # sprawdzanie pola
    # --------------------------------------------------------------------------------------------

    def check_cell(self, x1, y1, x2, y2):

        # czy dotarlismy do celu
        if x1 == x2 and y1 == y2:
            return True

        # czy wyszlismy poza plansze
        if x1 < 0 or x1 > BOARD_X - 1 or y1 < 0 or y1 > BOARD_Y - 1:
            return False

        # czy to pole jest zajete
        if self.board[y1][x1] != FIELD_EMPTY:
            return False

        # znakujemy pole i sprawdzamy sasiednie
        else:
            self.board[y1][x1] = FIELD_TESTED

            if self.check_cell(x1, y1 - 1, x2, y2) \
                    or self.check_cell(x1, y1 + 1, x2, y2) \
                    or self.check_cell(x1 - 1, y1, x2, y2) \
                    or self.check_cell(x1 + 1, y1, x2, y2):
                return True
            else:
                return False

    # --------------------------------------------------------------------------------------------
    # oczyszczanie planszy ze znacznikow sprawdzanych pol
    # --------------------------------------------------------------------------------------------

    def remove_marks(self):

        for row in range(BOARD_Y):
            for col in range(BOARD_X):
                if self.board[row][col] == FIELD_TESTED:
                    self.board[row][col] = FIELD_EMPTY

    # --------------------------------------------------------------------------------------------
    # usuwanie klockow
    # --------------------------------------------------------------------------------------------

    def remove_block(self, x, y, dx, dy):
        color = self.board[y][x]

        if DEBUG:
            if dx == 1 and dy == 0:
                print(" - ",)
            if dx == 0 and dy == -1:
                print(" | ",)
            if dx == 1 and dy == -1:
                print(" / ",)
            if dx == -1 and dy == -1:
                print(" \\ ",)
        count1 = count2 = 0

        # sprawdzanie w jedna strone
        a = x + dx
        b = y + dy
        while 0 <= a < BOARD_X and 0 <= b < BOARD_Y and self.board[b][a] == color:
            if DEBUG:
                print("(", a, b, self.board[b][a], ")",)
            count1 += 1
            a += dx
            b += dy
        if DEBUG:
            print(count1,)

        # sprawdzanie w druga strone
        a = x - dx
        b = y - dy
        while 0 <= a < BOARD_X and 0 <= b < BOARD_Y and self.board[b][a] == color:
            if DEBUG:
                print("(", a, b, self.board[b][a], ")",)
            count2 += 1
            a -= dx
            b -= dy
        if DEBUG:
            print(count2)

        # usuwanie w poziomie
        if count1 + count2 >= 4:
            # usuwanie w jedna strone
            if count1 > 0:
                if DEBUG:
                    print("USUWANIE W JEDNA STRONE")
                a = x + dx
                b = y + dy
                while 0 <= a < BOARD_X and 0 <= b < BOARD_Y and self.board[b][a] == color:
                    self.board[b][a] = 0
                    a += dx
                    b += dy
            # usuwanie w druga strone
            if count2 > 0:
                if DEBUG:
                    print("USUWANIE W DRUGA STRONE")
                a = x - dx
                b = y - dy
                while 0 <= a < BOARD_X and 0 <= b < BOARD_Y and self.board[b][a] == color:
                    self.board[b][a] = FIELD_EMPTY
                    a -= dx
                    b -= dy
            self.score += 10
            return True
        else:
            return False

    # --------------------------------------------------------------------------------------------

    def remove_all_blocks(self, x, y):

        color = self.board[y][x]

        if DEBUG:
            print("pos:", x, y, " | kolor:", color)

        if self.remove_block(x, y, 1, 0) \
                or self.remove_block(x, y, 0, -1) \
                or self.remove_block(x, y, -1, -1) \
                or self.remove_block(x, y, 1, -1):

            if DEBUG:
                print("... TRUE")

            self.board[y][x] = FIELD_EMPTY

            if self.skip == 0:
                self.skip = 1

            return True
        else:
            if DEBUG:
                print("... FALSE")

            return False
