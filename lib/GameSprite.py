import os

import pygame

# --------------------------------------------------------------------------------------------

DATA_DIR = "data"

# --------------------------------------------------------------------------------------------


class GameSprite(pygame.sprite.Sprite):

    _move = 0
    MIN_X = 0
    MAX_X = 600 - 191
    MIN_Y = 0
    MAX_Y = 400 - 218
    STEP = 2
    block = [0] * 3

    def __init__(self, filename, screen):

        self.screen = screen

        pygame.sprite.Sprite.__init__(self)
        self.img = pygame.image.load(os.path.join(DATA_DIR, filename)).convert()
        self.x = 0
        self.y = 0
        self.img.set_alpha(128)

    def draw(self):

        self.screen.blit(self.img, (self.x, self.y))

    def move(self):

        if self._move == pygame.K_LEFT:
            self.left()
        elif self._move == pygame.K_RIGHT:
            self.right()
        elif self._move == pygame.K_UP:
            self.up()
        elif self._move == pygame.K_DOWN:
            self.down()

    def right(self):

        self.x += self.STEP
        if self.x > self.MAX_X:
            self.x = self.MAX_X

    def left(self):

        self.x -= self.STEP
        if self.x < self.MIN_X:
            self.x = self.MIN_X

    def up(self):

        self.y -= self.STEP
        if self.y < self.MIN_Y:
            self.y = self.MIN_Y

    def down(self):

        self.y += self.STEP
        if self.y > self.MAX_Y:
            self.y = self.MAX_Y
