![pyKulki.png](https://bitbucket.org/repo/G4RE8n/images/2010441928-pyKulki.png)

# PL: pyKulki #

Bardzo stary projekt. Prawdopodobnie z 2009.

* bez virtualenv
* bez requirements.txt
* bez innych rzeczy

```
#!bash

$ python main.py
```

# GB: pyBalls #

Very old project. Probably from 2009.

* no virtualenv
* no requirements.txt
* no other things

```
#!bash

$ python main.py
```